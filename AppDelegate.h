//
//  AppDelegate.h
//  Ego
//
//  Created by Daniel Ungerfält on 28/01/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

