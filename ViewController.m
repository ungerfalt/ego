//
//  ViewController.m
//  Ego
//
//  Created by Daniel Ungerfält on 28/01/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import "ViewController.h"
CGPoint startOffset;
CGPoint destinationOffset;
NSDate *startTime;
NSTimer *timer;

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *colorButton;
@property(nonatomic) BOOL firstclick;


@end

@implementation ViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.firstclick = YES;
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"TESTING!!!");}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changDaColör:(id)sender {
    if(self.firstclick){
    self.view.backgroundColor = [UIColor greenColor];
        self.firstclick = NO;
    } else {
        self.view.backgroundColor = [UIColor redColor];
        self.firstclick = YES;
    }
}


@end
