//
//  main.m
//  Ego
//
//  Created by Daniel Ungerfält on 28/01/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
